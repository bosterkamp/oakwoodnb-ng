angular.module('app.services', [])

.service('Survey', ['$q', '$http', function($q, $http){

    console.log("Inside Survey Service");
    
    //Getting mixed content error...
    //var api_url = 'http://www.oakwoodnb.com/json/blog.php?name=verse-of-the-day';
    var api_url = 'https://sheetsu.com/apis/v1.0/293c6930e37d';
    var currentID = 1;

    var ret = {
        all: function(){
                console.log("Inside Survey Service all function");
            
            var deferred = $q.defer();
            
            /*
            return*/ $http.get(api_url).then(function(resp){
                console.log(resp);
                //if (resp.data.length > 0) currentID = parseInt(resp.data[resp.data.length-1].id);
                //return resp.data;
            });
            
            return deferred.promise;
            
        }, 
        add: function(data){
            currentID++;
            data.id = currentID;
            
            return $http.post(api_url, data).then(function(resp){
                return resp.data;
            });

        }
    }
    
    ret.all();
    
    return ret;

}]);

//angular.module('app.services', [])

//Verse of the Day Service - Commented out until I get it working.
//Feed URL http://www.oakwoodnb.com/mediafiles/verse-of-the-day.xml
/*
.service('Verse', ['$http', function($http){

    var api_url = 'http://www.oakwoodnb.com/mediafiles/verse-of-the-day.xml';
    
    var currentID = 1;

    var ret = {
        all: function(){
            
            return $http.get(api_url).then(function(resp){
                console.log(resp);
                if (resp.data.length > 0) currentID = parseInt(resp.data[resp.data.length-1].id);
                return resp.data;
            });
            
        }, 
        add: function(data){
            currentID++;
            data.id = currentID;
            
            return $http.post(api_url, data).then(function(resp){
                return resp.data;
            });

        }
    }
    
    ret.all();
    
    return ret;
}])

*/
/*
.service('BlankService', [function(){

}]);
*/

//Trying to get it working with Sheetsu and then we'll get it working with verse of the day
/*.service('Survey', ['$http', function($http){

    var api_url = 'https://sheetsu.com/apis/v1.0/293c6930e37d';
    var currentID = 1;

    var ret = {
        all: function(){
            
            return $http.get(api_url).then(function(resp){
                if (resp.data.length > 0) currentID = parseInt(resp.data[resp.data.length-1].id);
                return resp.data;
            });
            
        }, 
        add: function(data){
            currentID++;
            data.id = currentID;
            
            return $http.post(api_url, data).then(function(resp){
                return resp.data;
            });

        }
    }
    
    ret.all();
    
    return ret;

}]);
*/
