angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    

      .state('menu.oakwoodMobile', {
    url: '/page1',
    views: {
      'side-menu21': {
        templateUrl: 'templates/oakwoodMobile.html',
        controller: 'oakwoodMobileCtrl'
      }
    }
  })

  .state('menu.announcements', {
    url: '/page2',
    views: {
      'side-menu21': {
        templateUrl: 'templates/announcements.html',
        controller: 'announcementsCtrl'
      }
    }
  })

  .state('menu.verse', {
    url: '/page14',
    views: {
      'side-menu21': {
        templateUrl: 'templates/verse.html',
        controller: 'verseCtrl'
      }
    }
  })

  .state('menu', {
    url: '/side-menu21',
    templateUrl: 'templates/menu.html',
    controller: 'menuCtrl'
  })

  .state('menu.events', {
    url: '/page4',
    views: {
      'side-menu21': {
        templateUrl: 'templates/events.html',
        controller: 'eventsCtrl'
      }
    }
  })

  .state('menu.selectedSermon', {
    url: '/page5',
    views: {
      'side-menu21': {
        templateUrl: 'templates/selectedSermon.html',
        controller: 'selectedSermonCtrl'
      }
    }
  })

  .state('worshipGuide', {
    url: '/page6',
    templateUrl: 'templates/worshipGuide.html',
    controller: 'worshipGuideCtrl'
  })

  .state('menu.social', {
    url: '/page7',
    views: {
      'side-menu21': {
        templateUrl: 'templates/social.html',
        controller: 'socialCtrl'
      }
    }
  })

  .state('give', {
    url: '/page8',
    templateUrl: 'templates/give.html',
    controller: 'giveCtrl'
  })

  .state('feedback', {
    url: '/page9',
    templateUrl: 'templates/feedback.html',
    controller: 'feedbackCtrl'
  })

  .state('menu.sermons', {
    url: '/page10',
    views: {
      'side-menu21': {
        templateUrl: 'templates/sermons.html',
        controller: 'sermonsCtrl'
      }
    }
  })

  .state('menu.findUs', {
    url: '/page11',
    views: {
      'side-menu21': {
        templateUrl: 'templates/findUs.html',
        controller: 'findUsCtrl'
      }
    }
  })

  .state('menu.page', {
    url: '/page12',
    views: {
      'side-menu21': {
        templateUrl: 'templates/page.html',
        controller: 'pageCtrl'
      }
    }
  })

$urlRouterProvider.otherwise('/side-menu21/page1')


});